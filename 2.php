<?php
	$dbms = 'mysql';
	$dbname = 'person';
	$username = 'root';
	$password = '';
	$host = 'localhost:3306';
	$dsn = "$dbms:host=$host;dbname=$dbname";
	try{
		$pdo = new PDO($dsn,$username,$password);
		echo "";
	} catch(Exception $exc){
		echo $exc->getMessage();
	}
	//使用PDO编写PHP代码，实现对数据表tb_user的查询。使用echo输出每行记录的信息。
	$pdo->query('set names utf8');
	$result=$pdo->query('select * from message');
	$result->setFetchMode(PDO::FETCH_ASSOC);
	$i=1;
	while ($row=$result->fetch()) {
		foreach ($row as $key => $value) {
			echo $value." ";
			if($i%3 == 0){
				echo "<br />";
			}
			$i++;
		}
	}

?>
